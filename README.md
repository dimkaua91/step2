Список использованных технологий:
- Адаптивная верстка,
- JS,
- Sass,
- Сборка проекта Gulp.

Состав участников проекта:
Dmytro Gorobets & Vitalii Zhelobytsky


Какие задачи выполнял кто из участников:
Dmytro Gorobets:
- Секция Revolutionary Editor,
- Секция Here is what you get,
- Секция Fork Subscription Pricing.

Vitalii Zhelobytsky:
- Верстка шапки сайта с верхним меню (включая выпадающее меню при малом разрешении экрана),
- Секция People Are Talking About Fork.


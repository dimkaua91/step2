const burgerClosed = document.querySelector(".header__burger-closed");
const burgerMenu = document.querySelector(".header__list");
const burgerOpen = document.querySelector(".header__burger-open");
burgerClosed.addEventListener("click", function (){
        burgerMenu.classList.add("active");
        burgerClosed.classList.remove("active");
        burgerOpen.classList.add("active");
})
burgerOpen.addEventListener("click", function (){
    burgerMenu.classList.remove("active");
    burgerClosed.classList.add("active");
    burgerOpen.classList.remove("active");
});